# React-and-Springboot-data-rest
## Project Description
The application has a react frontend and a Spring Boot Rest API, packaged as a single module Maven application. You can build the application using maven and run it as a Spring Boot application using the flat jar generated in target (`java -jar target/*.jar`).

You can run the complete applicaiton by executing:
````
$mvn spring-boot:run
````
You can test the main API using the following curl commands (shown with its output):

````
$ curl -v -u greg:turnquist localhost:8080/api/employees/3
Response:
{
"firstName" : "Frodo",
"lastName" : "Baggins",
"description" : "ring bearer",
"manager" : {
"name" : "greg",
"roles" : [ "ROLE_MANAGER" ]
},
"\_links" : {
"self" : {
"href" : "http://localhost:8080/api/employees/1"
}
}
}

````

To see the frontend, navigate to http://localhost:8080. You are immediately redirected to a login form. Log in as <user/password>: `greg/turnquist`

---
## Gitlab CI/CD Pipeline to build and test the application and Deploy to Elastic Beanstalk
![cicd](/project-images/cicd.png)
To build, test and deploy the application Gitlab CI is used. The pipeline code can be found in .gitlab-ci.yml
##### Application Stack: Java, Springboot, React
##### CICD Tools/software used: apache maven, docker, gitlab ci, aws (s3 and elastic beanstalk) 
#####  Semver Versioning
To version the application semver versioning concept is used:
Given a version number x.y.z, increment the:
<br />x(MAJOR) version when you make incompatible API changes.
<br />y(MINOR) version when you add functionality in a backward compatible manner.
<br /> z(PATCH) version when you make backward compatible bug fixes.

Recent Version number is inserted on the top line in the file <b>semver.txt </b> starting with a # and <b>set_version.sh</b> script will set an environment variable ${appVersion} which will be used to give the application version number. This strategy is used for maven and docker build versioning.

pipeline_script/semver.txt
````
#1.0.0
Initial version

````
Example:
````
$source ./pipeline/set_version.sh #read the version number from semver.txt and set appVersion=1.0.0
$mvn clean install -DappVersion=${appVersion} #mvn sets application version to 1.0.0
````
Artifacts Tags based on branchname and commits:
<br/>Artifcats from "develop" branch gets names/version like: "develop-\${appVersion}-\${short-commit-id}" (example: develop-1.0.0-231acdef)
<br/>Artifcats from "release" branch gets names/version like: "rc-\${appVersion}-\${short-commit-id}" (example: rc-1.0.0-231acdef)
<br/>Artifcats from "main" branch gets names/versions like: ${appVersion} (example:1.0.0), this will be final prod version.
<br/>Name convention for Artifacts from feature branch can be done in similar fashion.

####  CI/CD Steps 
````
# Build the project
$mvn clean install -DappVersion=${appVersion}

# Test application
$mvn test
````

#### Package the application into Docker image and push to Container registry

For this project I used Gitlab inbuild Container registry to store the Dockerimages.
Container registry: registry.gitlab.com/tapas.rwth/spring-boot-react-rest-api/

#### Create a Dockerfile
Dockerfile
```
FROM openjdk:11.0.11-slim
WORKDIR /app
COPY target/react-and-spring-data-rest-*.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]
```
Script to build docker image ans push to registry
```
$docker login <docker_registry> -u <user_name> -p <password>
$docker build -t <docker_registry>:<image_tag_name> -f <Dockerfile_name> .
$docker push <docker_registry>:<image_tag_name>

```
#### Deploy to AWS Elastic Beanstalk

AWS Elastic Beanstalk is very useful aws service to orchestrate services like EC2, Elastic Loadbalancing, autoscaling, S3, IAM Roles and Policy, Cloud watch monitoring etc. Elastic Beanstalk reduces management complexity and helps to quickly create an application deployment environment and it support variaours technology stack/programming language. It automatically handles the details of capacity provisioning, load balancing, scaling, and application health monitoring. For the architecture please look in architecture section.

For this project I have choosen Docker Environment. For the deployment we need to package and application information and container registry login credentials and store to S3. These two files are 1.The appcation information file: <b>Dockerrun.aws.json</b> and 2.dockerlogin credentials file:<b>.dockercfgfile</b> which contains the token to login to private container registr. 

Deployment steps involved:
1. zip Dockerrun.aws.json file into app.zip and send to S3 with using command: "aws s3 cp app.zip s3://<S3_BUCKET_NAME>". A script (./deployment/deploy.sh) will generate these two files from a template.
2. send .dockercfgfile to S3 bucket using command: "aws s3 cp .dockercfgfile s3://<S3_BUCKET_NAME>"
3. create elasticbeanstalk application version using "aws elasticbeanstalk create-application-version ....  " command
4. update elasticbeanstalk update environment using "aws elasticbeanstalk update-environment --application-name ... " command

Gitlab CI runner docker image used to run the aws commands in pipeline: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest


Pipeline Environment Variable:
````
DOCKER_REGISTRY_TOKEN = Docker login tocken for this project's Container registry
S3_BUCKET_NAME = Bucket name where the necessary files is stored for application versioning
EB_APP_NAME = Elasticbeanstalk Application Name created in AWS
EB_ENV_NAME = Elasticbeanstalk Application Environment Name created under the above Elastic Beanstalk Application in AWS
AWS_ACCESS_KEY_ID = AWS User's access key id
AWS_SECRET_ACCESS_KEY = AWS User's secret access key token
AWS_DEFAULT_REGION = AWS Region
````
Commands used for Elastic Beanstalk deployment:
````
$source ./pipleine_script/deploy.sh ${S3_BUCKET_NAME} ${IMAGE_NAME} ${TAG_NAME} ${DOCKER_REGISTRY_TOKEN}
$zip <APP_NAME>.zip Dockerrun.aws.json
$aws s3 cp <APP_NAME>.zip s3://${S3_BUCKET_NAME}/app/
$aws s3 cp .dockercfgfile s3://${S3_BUCKET_NAME}/app/.dockercfgfile
$aws elasticbeanstalk create-application-version --application-name ${EB_APP_NAME} --version-label ${VERSION} --source-bundle S3Bucket=$S3_BUCKET_NAME,S3Key="app/<APP_NAME>.zip"
$aws elasticbeanstalk update-environment --application-name ${EB_APP_NAME} --environment-name ${EB_ENV_NAME} --version-label ${VERSION}
````

![pipeline view from gitlab](/project-images/pipeline_view.png)

#### Access the Application from Elastic Beanstalk

When the deployment to Elastic Beanstalk application environment is finished open the browser and copy the loadbalancer url/Beanstalk environemnt dns url and hit enter.You will see the application.

application login page             |  dashboard
:-------------------------:|:-------------------------:
![](/project-images/application.png){width=75%} |  ![](/project-images/app_dashboard.png){width=75%}

## Create Infrastructure using Terraform

AWS Infrastructure High Level design diagram
![architecture](/project-images/architecture.png)

Docker application will run in EC2 insatnces launched with a autoscaling group in 2 Public Subnets behind a Loadbalancer. According to load the autoscaling group can add more EC2 instances when it is required. EC2 and Loadbalancer metrics are monitoried in Cloudwatch. High Level design is presented in a separate High Level Design Document.

Infrastructure Repo: https://gitlab.com/tapas.rwth/aws-infra-with-terraform.git
Another pipeline is created from this repo.

To run the terraform code from local pc and create infra
````
available env: [dev, stage, prod]

# go to create_vpc_beanstalk folder in the above Repo
$ cd ./create_vpc_beanstalk

# initialize terraform
$terraform init

# create namespaces
$terraform workspace new dev
$terraform workspace new stage
$terraform workspace new prod

# list available workspaces
$terraform workspace list

# select namespace
$terraform workspace select <namespace_name>
example: $terraform workspace select dev

# Create Infrastructure
$terraform plan -var-file=<namespace_name>.tfvars
$terrafrom apply -var-file=<namespace_name>.tfvars

# Destroy Infrastructure
$terraform destroy -var-file=<namespace_name>.tfvars
````
#### How the application is hosted combining two pipelines:
![architecture](/project-images/app_hosting.png)
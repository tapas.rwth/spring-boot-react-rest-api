# Use a base image with Java 11
FROM openjdk:11.0.11-slim

# Set the working directory
WORKDIR /app
# Copy the JAR file to the container
COPY target/react-and-spring-data-rest-*.jar app.jar

# Expose the port that your Spring Boot application listens on (default is 8080)
EXPOSE 8080
# Define the entry command to run your application
CMD ["java", "-jar", "app.jar"]
#!/bin/bash
s3_bucket_name=$1
IMAGE_NAME=$2
TAG_NAME=$3
REGISTRY_TOKEN=$4
rm -rf Dockerrun.aws.json
rm -rf .dockercfgfile
#creating Dockerrun.aws.json file
cat << EOF >> Dockerrun.aws.json
{
    "AWSEBDockerrunVersion":"1",
    "Authentication":{
        "Bucket":"${s3_bucket_name}",
        "Key":"app/.dockercfgfile"

    },
    "Image":{
        "Name": "${IMAGE_NAME}:${TAG_NAME}"
    },
    "Ports":[
        {
            "ContainerPort":"8080"
        }
    ]
}
EOF
cat << EOF >> .dockercfgfile
{
        "auths": {
                "registry.gitlab.com": {
                        "auth": "${REGISTRY_TOKEN}"
                }
        }
}
EOF
